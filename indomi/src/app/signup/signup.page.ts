import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  constructor(private routes: Router, private authServie: AuthService) {}
  onLogin() {
    this.authServie.login();
    this.routes.navigateByUrl('/pages/tabs/dashboard');
  }

  ngOnInit() {}
}
