import { Injectable } from '@angular/core';
import { Drink, FastFood, Food, Sweets, Slide } from './pages.model';

@Injectable({
  providedIn: 'root',
})
export class PagesService {
  //Food
  private _food: Food[] = [
    new Food(
      'p1',
      'Mie Goreng Original',
      '',
      'https://cf.shopee.co.id/file/ddbe7d510c901ca02e76ea97cd168eab',
      5000
    ),
    new Food(
      'p2',
      'Mie Goreng Rendang',
      '',
      'https://cf.shopee.co.id/file/abdb5d59e458474dbbf5dd4ba0985ea3',
      2000
    ),
    new Food(
      'p3',
      'Mie Goreng Cabai Ijo',
      '',
      'https://cf.shopee.co.id/file/c6f74c40fa86c0f66d2639e9a16183ec',
      5000
    ),
    new Food(
      'p4',
      'Mie Goreng Sate',
      '',
      'https://cf.shopee.co.id/file/3b1f74c890d5cdc6397adb8da8bb1a47',
      5000
    )
  ];

  get food() {
    return [...this._food];
  }

  // Drink //
  private _drink: Drink[] = [
    new Drink(
      'p1',
      'Kopi Hitam',
      'Buah lemon segar dengan teh',
      'https://asset.kompas.com/crops/FtHUHmvhseDe4BYdfy5PoBaEVWo=/7x6:993x663/750x500/data/photo/2017/04/18/2438001086.jpg',
      9000
    ),
    new Drink(
      'p2',
      'Es Teh Manis',
      'Es teh manis, semanis senyumu',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/Es_teh_gelas_jumbo.jpg/220px-Es_teh_gelas_jumbo.jpg',
      5000
    ),
    new Drink(
      'p3',
      'Es Jeruk',
      'Segelas Es jeruk kualitas terbaik',
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSpSM3wRb5CJ_Q0Hv6M886Gir7UpXNsTceSOQ&usqp=CAU',
      6000
    ),
    new Drink(
      'p4',
      'Kopi Susu',
      'Jus Buah berkualitas',
      'https://asset-a.grid.id/crop/0x0:0x0/700x465/photo/2021/08/25/resep-kopi-susu-panas-kayumanis-20210825063021.jpg',
      6000
    ),
  ];

  get drink() {
    return [...this._drink];
  }

  // FastFood //
  private _fastfood: FastFood[] = [
    new FastFood(
      'p1',
      'Mi Kuah Empal Gentong',
      'Daging Sapi + Sayuran + keju + Mayones + saos',
      'https://cf.shopee.co.id/file/5eb3428abea40f99e0c8d700165325be',
      5000
    ),
    new FastFood(
      'p2',
      'Mi kuah cakalang',
      'Stick Potato',
      'https://cf.shopee.co.id/file/b7f037c9e602468a70d5cde046391900',
      5000
    ),
    new FastFood(
      'p3',
      'Mi Kuah Kari Ayam ',
      'Daging ayam + sayuran + saos + mayonese',
      'https://images.tokopedia.net/img/cache/700/product-1/2020/12/2/5520132/5520132_9697168f-811d-486a-aab9-89497e111d24.jpeg',
      5000
    ),
    new FastFood(
      'p4',
      'Mi Kuah Coto Makasar ',
      'Onion Ring + saos',
      'https://cf.shopee.co.id/file/f209bfc58d87bb0dc194b02ef60e3827',
      5000
    ),
  ];

  get fastfood() {
    return [...this._fastfood];
  }

  // Sweets //
  private _sweets: Sweets[] = [
    new Sweets(
      'p1',
      'Telor',
      'A creamy dessert is all you need after a fulfilling dinner',
      'https://cf.shopee.co.id/file/fe17813ae58e2c7e5a9dbe641bbc3e40',
      3500
    ),
    new Sweets(
      'p2',
      '"Sayur Caesim',
      'This beauty couldnt be any easier',
      'https://cf.shopee.co.id/file/c704ad398bb536dd94ba3c22571e1bd6',
      1000
    ),
    new Sweets(
      'p3',
      'Sosis',
      'Prefer vanilla',
      'https://awsimages.detik.net.id/community/media/visual/2020/06/19/sup-merah-sosis-sayuran-1_43.jpeg?w=480',
      15000
    ),
    new Sweets(
      'p4',
      'Bakso',
      'Classic',
      'https://img-global.cpcdn.com/recipes/0c4a5647a556354a/751x532cq70/bakso-sapi-asli-kenyal-foto-resep-utama.jpg',
      15000
    ),
  ];

  get sweets() {
    return [...this._sweets];
  }

  // Sweets //
  private _slide: Slide[] = [
    new Slide(
      'p1',
      'Banana Split + Ice Cream',
      'A creamy dessert is all you need after a fulfilling dinner',
      'https://static.toiimg.com/photo/52500416.cms',
      15000
    ),
    new Slide(
      'p2',
      '"Fried" Ice Cream',
      'This beauty couldnt be any easier',
      'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-friedicecream-072-1587757338.jpg?crop=1xw:1xh;center,top&resize=980:*',
      10000
    ),
    new Slide(
      'p3',
      'Vanilla Pudding',
      'Prefer vanilla',
      'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/20191210-vanilla-pudding-delish-ehg-6282-1577743542.jpg?crop=1xw:1xh;center,top&resize=980:*',
      15000
    ),
  ];

  get slide() {
    return [...this._slide];
  }

  constructor() {}
}
