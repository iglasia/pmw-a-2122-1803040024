import { Component, OnInit } from '@angular/core';
import { Drink, FastFood, Food } from '../pages.model';
import { PagesService } from '../pages.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {
  loadedDrink: Drink[];
  loadedFood: Food[];
  loadedFastfood: FastFood[];
  constructor(private pageService: PagesService) { }

  ngOnInit() {
    this.loadedDrink = this.pageService.drink;
    this.loadedFood = this.pageService.food;
    this.loadedFastfood = this.pageService.fastfood;
  }
  option = {
    slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    // autoPlay: true,
  };
}
