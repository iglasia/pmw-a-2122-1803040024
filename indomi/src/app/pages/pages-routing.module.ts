import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagesPage } from './pages.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: PagesPage,
    children: [
      {
        path: 'dashboard',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./dashboard/dashbord.module').then(
                (m) => m.DashbordPageModule
              ),
          },

          {
            path: 'food',
            loadChildren: () =>
              import('./dashboard/food/food.module').then(
                (m) => m.FoodPageModule
              ),
          },
          {
            path: 'drink',
            loadChildren: () =>
              import('./dashboard/drink/drink.module').then(
                (m) => m.DrinkPageModule
              ),
          },
          {
            path: 'fastfood',
            loadChildren: () =>
              import('./dashboard/fastfood/fasffood.module').then(
                (m) => m.FasffoodPageModule
              ),
          },
          {
            path: 'sweets',
            loadChildren: () =>
              import('./dashboard/sweets/sweets.module').then(
                (m) => m.SweetsPageModule
              ),
          },
        ],
      },
      {
        path: 'history',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./history/history.module').then(
                (m) => m.HistoryPageModule
              ),
          },
        ],
      },
      {
        path: 'order',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('./order/order.module').then((m) => m.OrderPageModule),
          },
        ],
      },
      {
        path: 'profil',
        loadChildren: () =>
          import('./profil/profil.module').then((m) => m.ProfilPageModule),
      },
      {
        path: '',
        redirectTo: '/pages/tabs/dashboard',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/pages/tabs/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'profil',
    loadChildren: () =>
      import('./profil/profil.module').then((m) => m.ProfilPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesPageRoutingModule {}
