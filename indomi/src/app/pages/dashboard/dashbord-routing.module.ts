import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashbordPage } from './dashbord.page';

const routes: Routes = [
  {
    path: '',
    component: DashbordPage,
  },
  {
    path: 'food',
    loadChildren: () =>
      import('./food/food.module').then((m) => m.FoodPageModule),
  },
  {
    path: 'drink',
    loadChildren: () =>
      import('./drink/drink.module').then((m) => m.DrinkPageModule),
  },
  {
    path: 'fastfood',
    loadChildren: () =>
      import('./fastfood/fasffood.module').then((m) => m.FasffoodPageModule),
  },
  {
    path: 'sweets',
    loadChildren: () =>
      import('./sweets/sweets.module').then((m) => m.SweetsPageModule),
  },  {
    path: 'migoreng',
    loadChildren: () => import('./migoreng/migoreng.module').then( m => m.MigorengPageModule)
  },
  {
    path: 'mikuah',
    loadChildren: () => import('./mikuah/mikuah.module').then( m => m.MikuahPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashbordPageRoutingModule {}
