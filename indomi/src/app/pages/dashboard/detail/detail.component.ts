import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { AuthService } from 'src/app/auth/auth.service';
import { CatatanComponent } from '../catatan/catatan.component';
import { PesananComponent } from '../pesanan/pesanan.component';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  currentModal = null;

  constructor(
    private modalController: ModalController,
    private routes: Router,
    private authServices: AuthService,
    private navCtrl: NavController
  ) {}

  ngOnInit() {}

  dashboard(){
    this.navCtrl.navigateBack('./pages/tabs/dashboard');
  }

  async dismis() {
    this.modalController.dismiss();

    // this.authServices.start();
    // this.routes.navigateByUrl('/pages/tabs/dashboard');
  }

  async presentModal(title, imageUrl, description, price, id) {
    const modal = await this.modalController.create({
      component: CatatanComponent,
      cssClass: 'modal',
      componentProps: {
        id: id,
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }


  async modalpesan(title, imageUrl, description, price, id) {
    const modal = await this.modalController.create({
      component: PesananComponent,
      cssClass: 'modal',
      componentProps: {
        id: id,
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }
}
