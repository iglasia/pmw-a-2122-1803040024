import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Sweets } from '../../pages.model';
import { PagesService } from '../../pages.service';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-sweets',
  templateUrl: './sweets.page.html',
  styleUrls: ['./sweets.page.scss'],
})
export class SweetsPage implements OnInit {
  loadedSweets: Sweets[];
  constructor(
    private pageService: PagesService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.loadedSweets = this.pageService.sweets;
  }

  option = {
    slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    // autoPlay: true,
  };
  async presentModal(title, imageUrl, description, price) {
    const modal = await this.modalController.create({
      component: DetailComponent,
      cssClass: 'modal',
      componentProps: {
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }
}
