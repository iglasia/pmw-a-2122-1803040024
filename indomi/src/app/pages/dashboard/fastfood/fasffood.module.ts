import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FasffoodPageRoutingModule } from './fasffood-routing.module';

import { FasffoodPage } from './fasffood.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FasffoodPageRoutingModule
  ],
  declarations: [FasffoodPage]
})
export class FasffoodPageModule {}
