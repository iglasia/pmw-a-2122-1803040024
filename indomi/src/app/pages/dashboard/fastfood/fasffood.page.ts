import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FastFood } from '../../pages.model';
import { PagesService } from '../../pages.service';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-fasffood',
  templateUrl: './fasffood.page.html',
  styleUrls: ['./fasffood.page.scss'],
})
export class FasffoodPage implements OnInit {
  loadedFastFood: FastFood[];
  constructor(private pageService: PagesService, private modalController: ModalController) {}

  ngOnInit() {
    this.loadedFastFood = this.pageService.fastfood;
  }

  option = {
    slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    // autoPlay: true,
  };

  async presentModal(title, imageUrl, description, price) {
    const modal = await this.modalController.create({
      component: DetailComponent,
      cssClass: 'modal',
      componentProps: {
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }
}
