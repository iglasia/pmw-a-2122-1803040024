import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FasffoodPage } from './fasffood.page';

const routes: Routes = [
  {
    path: '',
    component: FasffoodPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FasffoodPageRoutingModule {}
