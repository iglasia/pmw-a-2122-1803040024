import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-catatan',
  templateUrl: './catatan.component.html',
  styleUrls: ['./catatan.component.scss'],
})
export class CatatanComponent implements OnInit {

  constructor(private modalController: ModalController,) { }

  ngOnInit() {}

  async dismis() {
    this.modalController.dismiss();

    // this.authServices.start();
    // this.routes.navigateByUrl('/pages/tabs/dashboard');
  }
}
