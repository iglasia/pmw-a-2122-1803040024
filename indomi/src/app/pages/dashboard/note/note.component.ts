import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.scss'],
})
export class NoteComponent implements OnInit {

  constructor(private modalController: ModalController,private routes:Router,private navCtrl: NavController) { }
  async dismis() {
    this.modalController.dismiss();

    // this.authServices.start();
    // this.routes.navigateByUrl('/pages/tabs/dashboard');
  }
  ngOnInit() {}

  kembali(){
    this.navCtrl.navigateBack('/pages/tabs/dashboard');
  }

}
