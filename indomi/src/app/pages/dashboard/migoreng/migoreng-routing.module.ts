import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MigorengPage } from './migoreng.page';

const routes: Routes = [
  {
    path: '',
    component: MigorengPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MigorengPageRoutingModule {}
