import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Food, Slide } from '../../pages.model';
import { PagesService } from '../../pages.service';
import { DetailComponent } from '../detail/detail.component';
@Component({
  selector: 'app-migoreng',
  templateUrl: './migoreng.page.html',
  styleUrls: ['./migoreng.page.scss'],
})
export class MigorengPage implements OnInit {

  loadedFood: Food[];
  constructor(
    private pageService: PagesService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.loadedFood = this.pageService.food;
  }

  option = {
    slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    // autoPlay: true,
  };

  async presentModal(title, imageUrl, description, price) {
    const modal = await this.modalController.create({
      component: DetailComponent,
      cssClass: 'modal',
      componentProps: {
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }

}
