import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MigorengPageRoutingModule } from './migoreng-routing.module';

import { MigorengPage } from './migoreng.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MigorengPageRoutingModule
  ],
  declarations: [MigorengPage]
})
export class MigorengPageModule {}
