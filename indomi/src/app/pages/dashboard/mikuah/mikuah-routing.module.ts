import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MikuahPage } from './mikuah.page';

const routes: Routes = [
  {
    path: '',
    component: MikuahPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MikuahPageRoutingModule {}
