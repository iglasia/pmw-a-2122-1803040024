import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MikuahPageRoutingModule } from './mikuah-routing.module';

import { MikuahPage } from './mikuah.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MikuahPageRoutingModule
  ],
  declarations: [MikuahPage]
})
export class MikuahPageModule {}
