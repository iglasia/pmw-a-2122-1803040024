import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NoteComponent } from '../note/note.component';

@Component({
  selector: 'app-pesanan',
  templateUrl: './pesanan.component.html',
  styleUrls: ['./pesanan.component.scss'],
})
export class PesananComponent implements OnInit {

  constructor(private modalController: ModalController,) { }

  ngOnInit() {}
  async dismis() {
    this.modalController.dismiss();

    // this.authServices.start();
    // this.routes.navigateByUrl('/pages/tabs/dashboard');
  }
  async modalnote(title, imageUrl, description, price, id) {
    const modal = await this.modalController.create({
      component: NoteComponent,
      cssClass: 'modal',
      componentProps: {
        id: id,
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }
}
