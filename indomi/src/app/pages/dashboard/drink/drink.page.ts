import { identifierModuleUrl } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Drink } from '../../pages.model';
import { PagesService } from '../../pages.service';
import { DetailComponent } from '../detail/detail.component';

@Component({
  selector: 'app-drink',
  templateUrl: './drink.page.html',
  styleUrls: ['./drink.page.scss'],
})
export class DrinkPage implements OnInit {
  loadedDrink: Drink[];
  constructor(
    private pageService: PagesService,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.loadedDrink = this.pageService.drink;
  }

  option = {
    slidesPerView: 1.5,
    centeredSlides: true,
    loop: true,
    spaceBetween: 10,
    // autoPlay: true,
  };

  async presentModal(title, imageUrl, description, price, id) {
    const modal = await this.modalController.create({
      component: DetailComponent,
      cssClass: 'modal',
      componentProps: {
        id: id,
        title: title,
        imageUrl: imageUrl,
        description: description,
        price: price,
      },
    });
    return await modal.present();
  }
}
