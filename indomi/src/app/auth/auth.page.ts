import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.page.html',
  styleUrls: ['./auth.page.scss'],
})
export class AuthPage implements OnInit {
  constructor(private routes: Router, private authServie: AuthService) {}
  onLogin() {
    this.authServie.login();
    this.routes.navigateByUrl('/pages/tabs/dashboard');
  }
  onStart() {
    this.authServie.start();
    this.routes.navigateByUrl('/pages/tabs/dashboard');
  }
  ngOnInit() {}
}
