import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-startpage',
  templateUrl: './startpage.page.html',
  styleUrls: ['./startpage.page.scss'],
})
export class StartpagePage implements OnInit {
  constructor(private routes: Router, private authServie: AuthService) {}
  onStart() {
    this.authServie.start();
    this.routes.navigateByUrl('/pages/tabs/dashboard');
  }

  ngOnInit() {}
}
