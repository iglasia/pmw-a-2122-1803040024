import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-project-recipe',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
